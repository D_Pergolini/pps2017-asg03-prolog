
winner([X,Y,Z],win(X)):-X==Y,Y==Z.

completeRow([C1,C2,C3]):-ground(C1),ground(C2),ground(C3).

winnerByRow([R1,R2,R3],X):-completeRow(R1),winner(R1,X),!.
winnerByRow([R1,R2,R3],X):-completeRow(R2),winner(R2,X),!.
winnerByRow([R1,R2,R3],X):-completeRow(R3),winner(R3,X),!.

winnerByColumn([[C00,C01,C02],[C10,C11,C12],[C20,C21,C22]],X):-winnerByRow([[C00,C10,C20],[C10,C11,C21],[C20,C12,C22]],X).

winnerByDiag([[C00,_,_],[_,C11,_],[_,_,C22]],X):-winner([C00,C11,C22],X),!.

winnerByDiag([[_,_,C02],[_,C11,_],[C20,_,_]],X):-winner([C02,C11,C20],X),!.

getWinner([R1,R2,R3],X):-winnerByRow([R1,R2,R3],X),!.
getWinner([R1,R2,R3],X):-winnerByColumn([R1,R2,R3],X),!.
getWinner([R1,R2,R3],X):-winnerByDiag([R1,R2,R3],X),!.
getWinner([R1,R2,R3],even):-completeRow(R1),completeRow(R2),completeRow(R3),!.
getWinner([R1,R2,R3],nothing).

place([X,Y,Z],P,[C1,Y,Z]):-C1=P,not(ground(X)).
place([X,Y,Z],P,[X,C2,Z]):-C2=P,not(ground(Y)).
place([X,Y,Z],P,[X,Y,C3]):-C3=P,not(ground(Z)).
placeInTable([R1,R2,R3],P,[T,R2,R3]):-place(R1,P,T).
placeInTable([R1,R2,R3],P,[R1,T,R3]):-place(R2,P,T).
placeInTable([R1,R2,R3],P,[R1,R2,T]):-place(R3,P,T).

next(T,P,R,Tn):-placeInTable(T,P,Tn),getWinner(Tn,R).


%Secondarily, implement predicate:
%game(@Table,@Player,-Result,-TableList)
% TableList is the sequence of tables until Result win(x),
%win(o) or even
%game(T,P,R,[T]):-getWinner(T,X),X==win(_).
%game(T,P,R,X):-getWinner(T,X),X==nothing.
%game(T,P,R,[T]):-getWinner(T,X),X==even.
%next([[x,x,_],[_,_,_],[_,_,_]],x,R,L).
%game([[x,o,_],[_,_,_],[_,_,_]],x,R,L).

other(X,Y):-X==x,Y=o;X==o,Y=x.
game(T,P,win(Y),Ts):-next(T,P,win(Y),Ts),!.
game(T,P,even,Ts):-next(T,P,even,Ts),!.
game(T,P,R,[T,Ts,L]):-next(T,P,nothing,Ts),other(P,P2),game(Ts,P2,R,L).