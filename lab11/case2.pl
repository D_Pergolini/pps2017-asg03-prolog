fromList([_],[]).
fromList([H1,H2|T],[e(H1,H2)|L]):- fromList([H2|T],L).

% fromCircList(+List,-Graph)
%fromCircList([10,20,30],[e(10,20),e(20,30),e(30,10)]).
fromCircList([H1|T],Y):- fromCircList([H1|T],Y,H1).
fromCircList([H1,H2|T],[e(H1,H2)|L],F):- fromCircList([H2|T],L,F).
fromCircList([H],[e(H,T)],T).


dropAll([],X,[]).
dropAll([Y|T],X,Ys):-copy_term(X,X2),Y=X2,dropAll(T,X,Ys),!.
dropAll([H|T],X,[H|Y]):-dropAll(T,X,Y).

% dropNode(+Graph, +Node, -OutGraph)
% drop all edges starting and leaving from a Node
% use dropAll defined in 1.1
dropNode(G,N,O):- dropAll(G,e(N,_),G2),dropAll(G2,e(_,N),O).

% all the nodes that can be reached in 1 step from Node
% possibly use findall, looking for e(Node,_) combined
% with member(?Elem,?List)
%reaching([e(1,2),e(1,3),e(2,3)],1,L). -> L/[2,3]
%reaching([e(1,2),e(1,2),e(2,3)],1,L). -> L/[2,2]).
reaching(G,N,L):- findall(X,member(e(N,X),G),L).

% anypath(+Graph, +Node1, +Node2, -ListPath)
anypath(G,N1,N2,[e(N1,N2)]):-member(e(N1,N2),G).
anypath(G,N1,N2,A):-append([e(N1,N3)],L,A),member(e(N1,N3),G),anypath(G,N3,N2,L),!.

% allreaching(+Graph, +Node, -List)
% all the nodes that can be reached from Node
% Suppose the graph is NOT circular!
% Use findall and anyPath!
allreaching(G,N,X,L):- member(e(_,X),G),anypath(G,N,X,L).
allreaching(G,N,L):-findall(X,allreaching(G,N,X,Y),L).