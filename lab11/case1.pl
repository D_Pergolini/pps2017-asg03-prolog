% dropAny(?Elem,?List,?OutList)
dropAny(X,[X|T],T).
dropAny(X,[H|Xs],[H|L]):-dropAny(X,Xs,L).

%� dropFirst: drops only the first occurrence (showing no alternative results)
dropFirst(X,[X|T],T):-!.
dropFirst(X,[H|Xs],[H|L]):-dropFirst(X,Xs,L).

%dropLast: drops only the last occurrence (showing no alternative results)
dropLast(X,L,Ys):- reverse(Y,Ys),dropFirst(X,R,Y),reverse(L,R),!.

%dropAll: drop all occurrences, returning a single list as result
dropAll(X,[],[]).
dropAll(X,[X|T],Ys):-copy_term(X,X2),Y=X2,dropAll(X,T,Ys),!.
dropAll(X,[H|T],[H|Y]):-dropAll(X,T,Y).


