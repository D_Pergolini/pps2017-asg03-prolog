% same(List1,List2)
% are the two lists the same?
same([],[]).
same([X|Xs],[X|Ys]):- same(Xs,Ys).

%all_bigger(List1,List2)
% all elements in List1 are bigger than those in List2, 1 by 1
% example: all_bigger([10,20,30,40],[9,19,29,39])
all_bigger([],[]).
all_bigger([X|Xs],[Y|Ys]):- all_bigger(Xs,Ys),X>Y.


search(X,[X|_]).
search(X,[_|Xs]):-search(X,Xs).
% sublist(List1,List2)
% List1 should be a subset of List2
% example: sublist([1,2],[5,3,2,1])
sublist([],_).
sublist([H|T],Xs):-sublist(T,Xs),search(H,Xs).
