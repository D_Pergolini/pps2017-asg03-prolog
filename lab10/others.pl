% inv(List,List)
% example: inv([1,2,3],[3,2,1]).
inv([],[]).
inv([H|T],Y):-last(Ys,H,Y),inv(T,Ys).

% double(List,List)
% suggestion: remember predicate append/3
% example: double([1,2,3],[1,2,3,1,2,3]).
double(X,Y):-append(X,X,Y).

% times(List,N,List)
% example: times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).
times(Xs,1,Xs).
times(X,N,Y):- N > 0, N2 is N-1, append(X,Ys,Y),times(X,N2,Ys).

%proj(List,List)
% example: proj([[1,2],[3,4],[5,6]],[1,3,5]).
proj([],[]).
proj([[Y|_]|T],[Y|Xs]):-proj(T,Xs).