% size(List,Size)
% Size will contain the number of elements in List
size([],0).
size([_|T],M) :- size(T,N), M is N+1.

% size(List,Size)
% Size will contain the number of elements in List,
written using notation zero, s(zero), s(s(zero))
zero.
s(X).
sizeZ([],zero).
sizeZ([_|T],s(N)) :- sizeZ(T,N).

% sum(List,Sum)
sum([X|[]],X).
sum([X|T],S):-sum(T,N),S is N+X.

% average(List,Average)
% it uses average(List,Count,Sum,Average)
average(List,A) :- average(List,0,0,A).
average([],C,S,A) :- A is S/C.
average([X|Xs],C,S,A) :-
					C2 is C+1,
					S2 is S+X,
					average(Xs,C2,S2,A).
					
% max(List,Max)
% Max is the biggest element in List
% Suppose the list has at least one element
max([H|[]],H).
max([H|T],N):-max(T,N), H < N.
max([H|T],H):-max(T,N), H > N.
