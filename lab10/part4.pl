% seq(N,List)
% example: seq(5,[0,0,0,0,0]).
seq(0,[]).
seq(N,[0|T]):- N > 0, N2 is N-1, seq(N2,T).

% seqR(N,List)
% example: seqR(4,[4,3,2,1,0]).
seqR(0,[0]).
seqR(N,[N|T]):- N > 0, N2 is N-1, seqR(N2,T).

% seqR2(N,List)
% example: seqR2(4,[0,1,2,3,4]).
last([],X,[X]).
last([H|T],X,[H|Ys]):-last(T,X,Ys).

seqR2(0,[0]).
seqR2(N,Y):- N > 0, N2 is N-1,last(Ys,N,Y), seqR2(N2,Ys).
